<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $unique_key
 * @property string $description
 * @property string $chemistry
 * @property float $voltage_limit_min
 * @property float $voltage_limit_max
 * @property float $cells
 */
class Battery extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'unique_key',
        'chemistry',
        'description',
        'voltage_limit_min',
        'voltage_limit_max',
        'cells',
    ];
}
