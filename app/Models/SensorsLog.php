<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property float $main_bat_voltage
 */
class SensorsLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'main_bat_voltage',
    ];
}
