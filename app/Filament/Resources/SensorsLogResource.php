<?php

namespace App\Filament\Resources;

use App\Filament\Resources\SensorsLogResource\Pages;
use App\Filament\Resources\SensorsLogResource\RelationManagers;
use App\Models\SensorsLog;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SensorsLogResource extends Resource
{
    protected static ?string $model = SensorsLog::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make([
                    'sm' => 1,
                    'md' => 12,
                ])->schema([
                    // Main column
                    Card::make()
                        ->schema([

                            TextInput::make('main_bat_voltage')
                                ->required(),

                        ])

                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('main_bat_voltage')
                    ->label('Napěti na baterce [V]')
                    ->sortable('asc')
                    ->searchable(),
                TextColumn::make('updated_at')
                    ->label('Uloženo')
                    ->dateTime('d. m. Y - H:i')
                    ->sortable(),
            ])
            ->defaultSort('updated_at', 'desc')
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSensorsLogs::route('/'),
            'create' => Pages\CreateSensorsLog::route('/create'),
            'edit' => Pages\EditSensorsLog::route('/{record}/edit'),
        ];
    }
}
