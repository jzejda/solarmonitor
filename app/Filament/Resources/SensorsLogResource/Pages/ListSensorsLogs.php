<?php

namespace App\Filament\Resources\SensorsLogResource\Pages;

use App\Filament\Resources\SensorsLogResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSensorsLogs extends ListRecords
{
    protected static string $resource = SensorsLogResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
