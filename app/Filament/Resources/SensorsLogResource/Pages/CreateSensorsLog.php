<?php

namespace App\Filament\Resources\SensorsLogResource\Pages;

use App\Filament\Resources\SensorsLogResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSensorsLog extends CreateRecord
{
    protected static string $resource = SensorsLogResource::class;
}
