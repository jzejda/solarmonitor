<?php

namespace App\Filament\Resources\SensorsLogResource\Pages;

use App\Filament\Resources\SensorsLogResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSensorsLog extends EditRecord
{
    protected static string $resource = SensorsLogResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
