<?php

namespace App\Filament\Resources\SensorsLogResource\Widgets;

use Filament\Widgets\Widget;

class MainBatOverview extends Widget
{
    protected static string $view = 'filament.resources.sensors-log-resource.widgets.main-bat-overview';
}
