<?php

namespace App\Filament\Resources\BatteryResource\Pages;

use App\Filament\Resources\BatteryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListBatteries extends ListRecords
{
    protected static string $resource = BatteryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
