<?php

namespace App\Filament\Resources\BatteryResource\Pages;

use App\Filament\Resources\BatteryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditBattery extends EditRecord
{
    protected static string $resource = BatteryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
