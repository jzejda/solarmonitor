<?php

namespace App\Filament\Resources;

use App\Filament\Resources\BatteryResource\Pages;
use App\Models\Battery;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class BatteryResource extends Resource
{
    protected static ?string $model = Battery::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make([
                    'sm' => 1,
                    'md' => 12,
                ])->schema([
                    // Main column
                    Card::make()
                        ->schema([
                            TextInput::make('description')
                                ->label('Popis'),
                            TextInput::make('unique_key')
                                ->label('Identifikátor'),
                            TextInput::make('voltage_limit_min')
                                ->label('Min napětí.')
                                ->required(),
                            TextInput::make('voltage_limit_max')
                                ->label('Max napětí')
                                ->required(),
                            Select::make('chemistry')
                                ->label('Chemie')
                                ->options([
                                    'lifepo4' => 'LiFePO4',
                                    'louh' => 'Louhová'
                                ])
                                ->required(),

                            TextInput::make('cells')
                                ->label('Počet článků')
                                ->required(),
                        ])->columns(2)

                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('description')
                    ->label('Popis'),
                TextColumn::make('chemistry')
                    ->label('Typ'),
                TextColumn::make('voltage_limit_min')
                    ->label('Min. napětí [V]'),
                TextColumn::make('voltage_limit_max')
                    ->label('Max. baterky [V]'),
                TextColumn::make('cells')
                    ->label('Článků'),
                TextColumn::make('updated_at')
                    ->label('Uloženo')
                    ->dateTime('d. m. Y - H:i')
                    ->sortable(),
            ])
            ->defaultSort('updated_at', 'desc')
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBatteries::route('/'),
            'create' => Pages\CreateBattery::route('/create'),
            'edit' => Pages\EditBattery::route('/{record}/edit'),
        ];
    }
}
