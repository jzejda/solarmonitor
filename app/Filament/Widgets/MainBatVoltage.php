<?php

namespace App\Filament\Widgets;

use App\Models\SensorsLog;
use Carbon\Carbon;
use Filament\Widgets\LineChartWidget;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class MainBatVoltage extends LineChartWidget
{
    protected static ?int $sort = 2;

    public ?string $filter = '0day';

    protected static ?string $maxHeight = '400px';

    protected static ?string $pollingInterval = '30s';

    protected function getHeading(): string
    {
        return 'Napětí na baterce: ' . Carbon::now()->subDays($this->getDayBefore())->format('d.m.Y');
    }

    protected function getDayBefore()
    {
        $day = match ($this->filter) {
            '0day' => 0,
            '1day' => 1,
            '2day' => 2,
            '3day' => 3,
            '4day' => 4,
            '5day' => 5,
            '6day' => 6,
            '7day' => 7,
        };

        return $day;
    }

    protected function getFilters(): ?array
    {
        return [
            '0day' => 'Dnes',
            '1day' => Carbon::now()->subDays(1)->format('d.m.Y'),
            '2day' => Carbon::now()->subDays(2)->format('d.m.Y'),
            '3day' => Carbon::now()->subDays(3)->format('d.m.Y'),
            '4day' => Carbon::now()->subDays(4)->format('d.m.Y'),
            '5day' => Carbon::now()->subDays(5)->format('d.m.Y'),
            '6day' => Carbon::now()->subDays(6)->format('d.m.Y'),
            '7day' => Carbon::now()->subDays(7)->format('d.m.Y'),
        ];
    }

    protected static ?array $options = [

            'scales' => [
                'y' => [
                    'tick' => [
                        'stepSize' => 2, // minimum value
                    ],
                    'min' => 25,
                    'max' => 27.5,
                ]
            ]
    ];

    protected function getData(): array
    {
        $data = Trend::model(SensorsLog::class)
            ->between(
                start: now()->startOfDay()->subDays($this->getDayBefore()),
                end: now()->endOfDay()->subDays($this->getDayBefore()),
            )
            ->perHour()
            ->average('main_bat_voltage');

        return [
            'datasets' => [
                [
                    'label' => 'Napětí na betarce v',
                    'data' => $data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor' => 'rgb(75, 192, 192)',
                    'tension' => 0.1,
                ],
            ],
            'labels' => $data->map(fn (TrendValue $value) => Carbon::parse($value->date)->format('H:i')),
            'options' => [
                'scales' => [
                    'yAxes' => [

                            'display' => true,
                            'min' => '20',
                            'max' => '40',

                    ]
                ]
            ],

        ];
    }

    protected function getFormSchema(): array
    {

    }
}
