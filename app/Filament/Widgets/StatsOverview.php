<?php

declare(strict_types=1);

namespace App\Filament\Widgets;

use App\Models\Battery;
use App\Models\SensorsLog;
use Carbon\Carbon;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;
use Illuminate\Support\Facades\DB;

class StatsOverview extends BaseWidget
{
    protected static ?int $sort = 1;

    protected static ?string $pollingInterval = '30s';

    protected function getCards(): array
    {
        /** @var SensorsLog $lastLog */
        $lastLog = DB::table('sensors_logs')->orderBy('created_at', 'desc')->first();

        /* @var Battery $battery */
        $battery = Battery::where('unique_key', '=', 'bat1')->first();

        if (!is_null($battery)) {
            $maxBatVoltage = ($battery->voltage_limit_max) * $battery->cells;
            $minBatVoltage = ($battery->voltage_limit_min) * $battery->cells;
        } else {
            $maxBatVoltage = 28.4;
            $minBatVoltage = 25.2;
        }

        $batCapacity = ((floatval($lastLog->main_bat_voltage - $minBatVoltage)) * 100) / ($maxBatVoltage - $minBatVoltage);
        $sensor = DB::table('sensors_logs')->select('main_bat_voltage')->limit(10)->orderBy('created_at', 'desc')->get();

        $chargingStatus = 'primary';
        $chargingText = '';
        $chargingIcon = 'heroicon-s-arrow-path';

        $data = [];
        if ($sensor->isNotEmpty()) {
            foreach (json_decode(json_encode($sensor), true) as $item) {
                $data[] = $item['main_bat_voltage'];
            }

            $elements = count($data);

            if ($elements > 2) {
                $actualElement = $data[0];
                $lastElement = $data[$elements - 1];

                if ($actualElement > $lastElement) {
                    $chargingStatus = 'success';
                    $chargingText = 'Nabíjení';
                    $chargingIcon = 'heroicon-s-trending-up';
                } else {
                    $chargingStatus = 'danger';
                    $chargingText = 'Vybíjení';
                    $chargingIcon = 'heroicon-s-trending-down';
                }
            }
        }

        return [
            Card::make('Nabytí baterky [%]', round($batCapacity, 1))
                ->description($chargingText)
                ->descriptionIcon($chargingIcon)
                ->chart(array_reverse($data))
                ->color($chargingStatus),
            Card::make('Napětí baterka / článek [V]', $lastLog->main_bat_voltage !== null ? $lastLog->main_bat_voltage . ' / ' . round(($lastLog->main_bat_voltage / 8), 2) : 0),
        ];
    }
}
