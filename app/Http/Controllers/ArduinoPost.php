<?php

namespace App\Http\Controllers;

use App\Models\SensorsLog;
use Illuminate\Http\Request;

class ArduinoPost extends Controller
{

    public function store(Request $request): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
    {
        $mainVoltage = $request->input('stringThree');

        $log = new  SensorsLog();
        $log->main_bat_voltage = $mainVoltage;
        $log->save();

        return response('OK', 200);
    }
}
