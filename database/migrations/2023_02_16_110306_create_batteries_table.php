<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('batteries', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->text('unique_key')->nullable();
            $table->text('chemistry')->nullable();
            $table->text('description')->nullable();
            $table->float('voltage_limit_min')->nullable();
            $table->float('voltage_limit_max')->nullable();
            $table->integer('cells')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('batteries');
    }
};
